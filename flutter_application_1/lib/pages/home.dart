import 'package:flutter/material.dart';
import 'package:flutter_application_1/controllers/counterController.dart';
import 'package:flutter_application_1/pages/other.dart';
import 'package:get/get.dart';

class HomeScreen extends StatelessWidget {
  final CounterController counterController = Get.put(CounterController());
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      Center(child: Text("Clicks:${counterController.counter.value}")),
      SizedBox(height: 10),
      ElevatedButton(
          onPressed: () {
            Get.to(OtherScreen());
          },
          child: Text("Open other page"))
    ]));
  }
}
